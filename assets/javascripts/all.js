var app = angular.module('app', ['ngRoute', 'duScroll']);

app.config(function($routeProvider, $locationProvider) {
	$locationProvider.html5Mode(true);
	$routeProvider

	.when('/', {
		templateUrl : 'app/views/home.html',
		controller : 'homeController'
	})

	.otherwise ( { redirectTo: '/' } );
});
app.controller('homeController', function($rootScope, $scope, $routeParams, $document) {
	$rootScope.showLogin = false;
	function setSectionHeight() {
		$('.section').height($(window).height());
	};
	
	$(window).on('resize', setSectionHeight);
	setSectionHeight();
	
	var sections = new Array();
	var currentSection;
	
	for (var index = 0; index < $('.section').length; index++) {
		currentSection = $('.section').eq(index);
		sections.push(currentSection);
		$('<div class="circles"></div>').appendTo(currentSection);
		
		for (var k = 0; k < 12; k++) {
			currentSection.find('.circles').append('<span class="circle">');	
		}
	}

	$scope.closeModal = function($event){
		$($event.target).parents('.modal').removeClass('active');
	};

	$scope.openModal = function($event){
		$($event.target).parents('.section').find('.modal').addClass('active');
	};
	
	$document.on('scroll', function() {	
		for (var index = 0; index < sections.length; index++) {
			var element = sections[index];
			
			if ($document.scrollTop() + 1 <= element.offset().top + element.height()) {
				$('#header').removeClass().addClass(element.attr('id'));
				break;
			}
		}
	});

	$('.carousel').owlCarousel({
		loop:true,
		margin:10,
		nav:true,
		navText: ['<', '>'],
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			},
			1000:{
				items:4
			}
		}
	})
});