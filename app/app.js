var app = angular.module('app', ['ngRoute', 'duScroll']);

app.config(function($routeProvider, $locationProvider) {
	$locationProvider.html5Mode(true);
	$routeProvider

	.when('/', {
		templateUrl : 'app/views/home.html',
		controller : 'homeController'
	})

	.otherwise ( { redirectTo: '/' } );
});