module.exports = function(grunt) {

	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),

		config : {
			app_dir : 'app',
			src_dir : 'sources',
			assets_dir : 'assets',
			banner : [
				'/*',
				' * <%= pkg.name %> - <%= pkg.version %>',
				' * Copyright (c) <%= grunt.template.today("yyyy") %> Moisés Pio',
				' */\n'
			].join('\n')
		},

		concat : {
			plugins : {
				src : [
					'<%=config.src_dir%>/javascripts/vendor/angular.min.js',
					'<%=config.src_dir%>/javascripts/vendor/angular-route.min.js',
					'<%=config.src_dir%>/javascripts/vendor/*.js'
				],
				dest : '<%=config.assets_dir%>/javascripts/vendor/vendor.js'
			},
			dist : {
				src : ['<%=config.app_dir%>/**/*.js'],
				dest : 'assets/javascripts/all.js'
			}
		},

		uglify : {
			options : {
				banner : '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
				mangle : false
			},
			dist : {
				files : {
					'<%=config.assets_dir%>/javascripts/all.min.js' : ['<%= concat.dist.dest %>'],
					'<%=config.assets_dir%>/javascripts/vendor/vendor.min.js' : ['<%= concat.plugins.dest %>']
				}
			}
		},

		watch : {
			options : {
				spawn : false,
				livereload : true
			},
			scripts : {
				files : '<%=config.app_dir%>/**/*.js',
				tasks : ['concat','uglify']
			},
			plugins : {
				files : '<%=config.src_dir%>/javascripts/vendor/**/*.js',
				tasks : ['concat','uglify']
			},
			css : {
				options : {
					spawn : true,
					livereload : true
				},
				files : ['<%=config.src_dir%>/**/*.scss'],
				tasks : ['sass']
			}
		},

		sass: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%=config.src_dir%>/stylesheets',
					src: ['*.scss'],
					dest: '<%=config.assets_dir%>/stylesheets',
					ext: '.css'
				}]
			}
		}

	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-concat');	
	grunt.loadNpmTasks('grunt-contrib-uglify');	

	grunt.registerTask('dev', ['concat', 'uglify', 'sass', 'watch']);
};